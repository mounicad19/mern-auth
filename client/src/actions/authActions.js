import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import jwt_decode from "jwt-decode";
import { GET_ERRORS, SET_CURRENT_USER, USER_LOADING } from "./types";

export const registerUser = (userData, history) => dispatch => {
    axios.post("/api/users/register", userData).then(resp => {
        history.push("/login");
    }).catch(function (err) {
        dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        })
    })
}

export const loginUser = (userData) => dispatch => {
    axios.post("/api/users/login", userData).then(res => {
        const { token } = res.data;
        localStorage.setItem('jwtToken', token);
        setAuthToken(token);
        try {
            const decodedToken = jwt_decode(token);
            dispatch(setCurrentUser(decodedToken))
        } catch (error) {
            console.log(' invalid token format', error);
            return true;
        }


    }).catch(function (err) {
        console.log('err', err)
        dispatch({
            type: GET_ERRORS,
            payload: err.response.data
        })
    })
}

export const setCurrentUser = decodedToken => {
    return {
        type: SET_CURRENT_USER,
        payload: decodedToken
    }
}

// User loading
export const setUserLoading = () => {
    return {
        type: USER_LOADING
    };
};

export const logoutUser = () => (dispatch) => {
    localStorage.removeItem('jwtToken');
    setAuthToken(false);
    dispatch(setCurrentUser({}))
}